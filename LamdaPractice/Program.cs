﻿using LamdaPractice.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new DatabaseContext())
            {
                // Write your code
                // Prefereable one function for each query
                // ctx.Cities.ToList().ForEach(c => Console.WriteLine(c.Name));

                DatabaseContext db = new DatabaseContext();

                Console.WriteLine("------------------------------------------------------------------------");
                Console.WriteLine("Listar todos los empleados cuyo departamento tenga una sede en Chihuahua");
                Console.WriteLine("------------------------------------------------------------------------");
                //Listar todos los empleados cuyo departamento tenga una sede en Chihuahua
                var result = db.Cities.First(ds => ds.Name == "Chihuahua");
                var result2 = db.Employees.ToList().FindAll(c => c.CityId == result.Id);
                result2.ForEach(ec => Console.WriteLine(ec.FirstName + " " + ec.LastName));

                Console.WriteLine("------------------------------------------------------------------------");
                Console.WriteLine("Listar todos los departamentos y el numero de empleados que pertenezcan a cada departamento.");
                Console.WriteLine("------------------------------------------------------------------------");
                //Listar todos los departamentos y el numero de empleados que pertenezcan a cada departamento.
                var result3 = db.Departments.ToList();
                var result4 = db.Employees.ToList();
                result3.ForEach(d => Console.WriteLine("Departemento de: " + d.Name + " # de empleados: "+ result4.Where(e => e.DepartmentId == d.Id).Count()));

                Console.WriteLine("---------------------------------------------------------------------------");
                Console.WriteLine("Listar todos los empleados remotos. Estos son los empleados cuya ciudad no se encuentre entre las sedes de su departamento.");
                Console.WriteLine("----------------------------------------------------------------------------");
                //Listar todos los empleados remotos. Estos son los empleados cuya ciudad no se encuentre entre las sedes de su departamento.
                //pendiente

                Console.WriteLine("--------------------------------------------------------------------------------");
                Console.WriteLine("Listar todos los empleados cuyo aniversario de contratación sea el próximo mes.");
                Console.WriteLine("--------------------------------------------------------------------------------");
                //Listar todos los empleados cuyo aniversario de contratación sea el próximo mes.
                
                var result5 = db.Employees.ToList();
                var result6 = result5.FindAll(em => em.HireDate.Month == 5);
                result6.ForEach(f => Console.WriteLine("Fecha aniversario:  " + f.HireDate + "--- "+ f.FirstName + " " + f.LastName));

                Console.WriteLine("-------------------------------------------------------------------------------");
                Console.WriteLine("Listar los 12 meses del año y el numero de empleados contratados por cada mes.");
                Console.WriteLine("-------------------------------------------------------------------------------");
                //Listar los 12 meses del año y el numero de empleados contratados por cada mes
                var result7 = db.Employees.ToList();
                for (int mes = 1; mes <= 12; mes++){
                Console.WriteLine("Mes # " + mes + " contratados " + result7.FindAll(n => n.HireDate.Month == mes).Count() + " empleados");
                }

                //Otra forma- empleando group by y select para demostrar su uso sin embargo mas facil la anterior
                //var result7 = db.Employees.GroupBy(m => m.HireDate.Month).ToList();
                //var result8 = result7.Select(c => new { ID = c.Key, count = c.Count() }).ToList();
                //var result9 = result8.AsEnumerable().OrderBy(e => e.ID).ToList();
                //result9.ForEach(e => Console.WriteLine("Mes # : " + e.ID + " Contratados:  " + e.count + " empleados"));
                
            }

            Console.Read();
        }
    }
}
